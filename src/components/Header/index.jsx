import React from 'react';
import { Input, Row, Col, Avatar } from 'antd';
import { SearchOutlined, UserOutlined } from '@ant-design/icons';
import { debounce } from 'lodash';
import useGlobal from '../../services/useGlobal';
import './style.scss';

const Header = () => {
	const [, gAction] = useGlobal();

	const onChangeHandler = debounce((e) => {
		gAction.searching(e.target.value);
	}, 500);

	return (
		<header className='header__child'>
			<Row justify='space-between' align='middle'>
				<Col xs={15} sm={18}>
					<Input
						placeholder='Search...'
						prefix={<SearchOutlined />}
						size='large'
						className='header__search'
						onChange={onChangeHandler}
					/>
				</Col>
				<Col xs={9} sm={6}>
					<div className='header__avatar'>
						<Avatar size={40} icon={<UserOutlined />} />
					</div>
				</Col>
			</Row>
		</header>
	);
};

export default Header;
