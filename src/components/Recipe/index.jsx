import React from 'react';
import { useHistory } from 'react-router-dom';
import { Image, Tooltip, Divider } from 'antd';
import { ClockCircleOutlined, DashboardFilled } from '@ant-design/icons';
import './style.scss';

const Recipe = (props) => {
	const history = useHistory();

	const { recipe } = props;
	const {
		title,
		description,
		images,
		servings,
		prepTime,
		cookTime,
		uuid,
	} = recipe;

	const onClickHandler = () => {
		history.push(`${uuid}`);
	};

	return (
		<div className='recipe__item' onClick={onClickHandler}>
			<div className='recipe__item-img'>
				<Image src={`http://localhost:3001/${images.full}`} preview={false} />
			</div>
			<div className='recipe__item-details'>
				<div className='recipe__item-title'>
					<span>{title}</span>
				</div>
				<div className='recipe__item-description'>
					<span>{description}</span>
				</div>
				<div className='recipe__item-preparation'>
					<span className='recipe__item-icon'>
						<Tooltip title='Cooking Time'>
							<DashboardFilled /> {cookTime}
						</Tooltip>
					</span>
					<span className='recipe__item-icon'>
						<Tooltip title='Preparation Time'>
							<ClockCircleOutlined /> {prepTime}'
						</Tooltip>
					</span>
					<span>{servings} Servings</span>
				</div>
			</div>
			<Divider />
		</div>
	);
};

export default Recipe;
