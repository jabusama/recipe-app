import React from 'react';
import { Spin } from 'antd';

const Loading = (props) => {
	const { isloading = true } = props;
	return (
		<div
			className='loading'
			style={{
				height: '93vh',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<Spin spinning={isloading} size='large' />
		</div>
	);
};

export default Loading;
