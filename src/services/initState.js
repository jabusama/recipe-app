const initState = {
	recipes: [],
	specials: [],
	recipesLoaded: false,
	specialsLoaded: false,
	loading: false,
	search: '',
};

export default initState;
