import { apiRequest } from './api';

const actions = {
	getRecipes: async (store) => {
		store.setState({ loading: true });
		const res = await apiRequest('GET', 'recipes');
		if (res.data.length !== 0) {
			await store.setState({
				recipes: res.data,
				recipesLoaded: true,
				loading: false,
			});
		} else {
			store.setState({ loading: false });
		}
	},

	getSpecials: async (store) => {
		store.setState({ loading: true });
		const res = await apiRequest('GET', 'specials');
		if (res.data.length !== 0) {
			await store.setState({
				specials: res.data,
				specialsLoaded: true,
				loading: false,
			});
		} else {
			store.setState({ loading: false });
		}
	},

	searching: async (store, text) => {
		store.setState({ loading: true });
		setTimeout(() => {
			store.setState({ search: text, loading: false });
		}, 300);
	},
};

export default actions;
