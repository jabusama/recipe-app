import axios from 'axios';

// export const apiUrl = process.env.REACT_APP_API_URL;

export const apiRequest = async (
	method = 'GET',
	url = '',
	data = {},
	header = {}
) => {
	try {
		return axios({
			method,
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				...header,
			},
			url: apiUrl + url,
			data,
		})
			.then((res) => {
				return res;
			})
			.catch((err) => {
				return err.response;
			});
	} catch (e) {
		console.error('API request Failed');
		return false;
	}
};

export const apiUrl = 'http://localhost:3001/';
