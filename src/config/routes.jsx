import Home from '../pages/Home';
import Preview from '../pages/Preview';

export const routes = [
	{
		path: '/',
		label: 'Home',
		key: 'home',
		isHidden: false,
		exact: true,
		component: Home,
	},
	{
		path: '/:id',
		label: 'Preview',
		key: 'preview',
		isHidden: false,
		component: Preview,
	},
];
