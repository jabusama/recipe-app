import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Image, Tooltip, Divider, Radio, List, Spin } from 'antd';
import {
	ClockCircleFilled,
	DashboardFilled,
	CaretLeftFilled,
	FileTextFilled,
	PushpinFilled,
	SignalFilled,
} from '@ant-design/icons';
import useGlobal from '../../services/useGlobal';
import Loading from '../../components/Loading';
import { isEmpty } from 'lodash';
import './style.scss';

const PreviewRecipe = () => {
	const history = useHistory();
	const [gState, gActions] = useGlobal();
	const [recipe, setRecipe] = useState({});
	const [tab, setTab] = useState(1);
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		if (!gState.recipesLoaded) {
			gActions.getRecipes();
		}
		if (!gState.specialsLoaded) {
			gActions.getSpecials();
		}
	});

	useEffect(() => {
		if (gState.recipes.length !== 0) {
			gState.recipes.map((item) => {
				if (item.uuid === history.location.pathname.replace('/', '')) {
					setRecipe(item);
				}
			});
		}
	});

	const renderSpecial = (id) => {
		let special = [];
		gState.specials.map((item) => {
			if (item.ingredientId === id) {
				special.push(item);
			}
		});
		return special;
	};

	const renderTitle = (item) => {
		if (tab === 1) {
			return (
				<div style={{ fontSize: 16 }}>
					<div className='ingredients__title' style={{ marginBottom: '.5rem' }}>
						{`${item.amount} ${item.measurement} ${item.name}`}
					</div>
					<div>
						{renderSpecial(item.uuid).map((special) => {
							return (
								<div
									key={special.uuid}
									style={{ fontWeight: 400, marginLeft: '1rem' }}
								>
									<span style={{ marginRight: 16 }}>
										<PushpinFilled /> {special.title}
									</span>
									<span>
										<SignalFilled /> {special.type}
									</span>
									<div>
										<FileTextFilled /> {special.text}
									</div>
								</div>
							);
						})}
					</div>
				</div>
			);
		} else {
			return (
				<div>
					{!item.optional && (
						<span
							style={{ color: '#ff0000', position: 'absolute', left: '-10px' }}
						>
							*
						</span>
					)}
					{item.instructions}
				</div>
			);
		}
	};

	const onChangeTab = (e) => {
		setIsLoading(true);
		setTimeout(() => {
			setTab(e.target.value);
			setIsLoading(false);
		}, 500);
	};

	return isEmpty(recipe) ? (
		<Loading isLoading={true} />
	) : (
		<div className='preview'>
			<div className='preview__button'>
				<CaretLeftFilled onClick={() => history.goBack()} />
			</div>

			<div className='recipe__header-img'>
				<Image
					src={`http://localhost:3001/${recipe.images.full}`}
					preview={false}
				/>
			</div>

			<div className='preview__item'>
				<div className='preview__details'>
					<div className='recipe__item'>
						<div className='recipe__item-details'>
							<div className='recipe__item-title'>
								<span>{recipe.title}</span>
							</div>
							<div className='recipe__item-description'>
								<span>{recipe.description}</span>
							</div>
							<div className='recipe__item-preparation'>
								<span className='recipe__item-icon'>
									<Tooltip title='Cooking Time'>
										<DashboardFilled /> {recipe.cookTime}
									</Tooltip>
								</span>
								<span className='recipe__item-icon'>
									<Tooltip title='Preparation Time'>
										<ClockCircleFilled /> {recipe.prepTime}'
									</Tooltip>
								</span>
								<span>{recipe.servings} Servings</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className='preview__tabs'>
				<Divider />
				<div className='preview__tabs-group'>
					<Radio.Group defaultValue={tab} onChange={onChangeTab}>
						<Radio.Button value={1}>Ingredients</Radio.Button>
						<Radio.Button value={2}>Preparations</Radio.Button>
					</Radio.Group>
				</div>
				<div className='preview__tabs-content'>
					<Spin spinning={isLoading} size='large'>
						<List
							itemLayout='horizontal'
							dataSource={tab === 1 ? recipe.ingredients : recipe.directions}
							renderItem={(item) => (
								<List.Item>
									<List.Item.Meta
										className='preview__tabs-list'
										title={<span>{renderTitle(item)}</span>}
									/>
								</List.Item>
							)}
						/>
					</Spin>
				</div>
			</div>
		</div>
	);
};

export default PreviewRecipe;
