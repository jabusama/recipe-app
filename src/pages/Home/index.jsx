import React, { useEffect, useState } from 'react';
import useGlobal from '../../services/useGlobal';
import { Button, Spin, Empty } from 'antd';
import Recipe from '../../components/Recipe';
import Header from '../../components/Header';
import './style.scss';

const Home = () => {
	const [gState, gActions] = useGlobal();
	const [recipeList, setRecipeList] = useState([]);
	useEffect(() => {
		if (!gState.recipesLoaded) {
			gActions.getRecipes(gState.recipes);
		}
	}, []);

	useEffect(() => {
		setRecipeList(gState.recipes);
	}, [gState.recipes]);

	useEffect(() => {
		if (gState.search) {
			setRecipeList(searchHandler());
		} else {
			setRecipeList(gState.recipes);
		}
	}, [gState.search]);

	const searchHandler = () => {
		return gState.recipes.filter(
			(recipe) =>
				JSON.stringify(recipe.title).search(new RegExp(gState.search, 'ig')) >
				-1
		);
	};

	const renderRecipes = () => {
		if (recipeList.length === 0) {
			return <Empty />;
		}

		return recipeList.map((item) => (
			<div key={item.uuid}>
				<Recipe recipe={item} />
			</div>
		));
	};

	return (
		<>
			<div className='header'>
				<Header />
			</div>
			<div className='recipe__content'>
				<div className='recipe__category'>
					<div className='recipe__btn'>
						<Button type='link'>ALL RECIPE</Button>
						<span className='recipe__bottom-border'>&nbsp;</span>
					</div>
				</div>

				<div className='recipe__list'>
					<Spin spinning={gState.loading}>{renderRecipes()}</Spin>
				</div>
			</div>
		</>
	);
};

export default Home;
